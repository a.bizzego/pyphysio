import numpy as np
from pyphysio.loaders import load_nirx2
from pyphysio.specialized.fnirs import SignalQualityDeepLearning
from pyphysio.segmenters import FixedSegments, fmap
import pyphysio.artefacts as artefacts
from pyphysio.specialized.fnirs import Raw2Oxy, NegativeCorrelationFilter, SDto1darray
import pyphysio.filters as filters

import matplotlib.pyplot as plt

ratio_min_good = 0.8 #at least 80% of the windows should have a good value
segmenter = FixedSegments(10, 20)

indicators = [SignalQualityDeepLearning()]

DATA_FOLDER = '/home/bizzego/UniTn/data/fnirs_technical_validation/hyper/pilot'
OUT_FOLDER = '/home/bizzego/tmp'

#%%
hb_ = []
for subject in ['a', 'b']:
    nirs = load_nirx2(f'{DATA_FOLDER}/TN001/TN001_base/tn001f{subject}_001') 

    plt.figure()
    nirs.p.plot(sharey=False)

    if np.sum(np.isnan(nirs.p.main_signal.values)) > 0:
        nirs = nirs.process_na('impute')
        
    #% SQI using Deep Learning
    sqi = fmap(segmenter, indicators, nirs)
    sqi = sqi.drop_vars(['component_start', 'component_stop', 'label'])
    sqi = sqi.sel(component = [0])
    
    # select channels
    isgood_vals = sqi['nirs_SignalQualityDeepLearning_isgood'].values[:,:,0]
    isgood_ratios = np.sum(isgood_vals, axis=0)/isgood_vals.shape[0]
    id_good_channels = np.where(isgood_ratios >= ratio_min_good)[0]
    
    print(subject + " good channels: ", id_good_channels)

    #% preprocessing
    nirs = artefacts.MARA()(nirs)
    nirs = artefacts.WaveletFilter()(nirs)
    
    #% convert to hb
    hb = Raw2Oxy(age=21)(nirs)
    
    #% filters
    hb = filters.FIRFilter(fp = [0.01, 0.2], fs = [0, 0.5])(hb)
    hb = NegativeCorrelationFilter()(hb)
    
    #%plot
    hb_.append(hb)

    #% save
    hb = SDto1darray(hb)
    hb.attrs['good_channels'] = id_good_channels
    hb.to_netcdf(f'{OUT_FOLDER}/hb_{subject}')