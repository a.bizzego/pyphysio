# pyphysio

pyphysio is a library of state of art algorithms for the analysis of physiological signals.
It contains the implementations of the most important algorithms for the analysis of physiological data like ECG, BVP, EDA, inertial, and fNIRS.

### Install
You can install `pyphysio` using `pip`:

`pip install pyphysio`

### Examples
Examples on how to use pyphysio can be found at:

<https://gitlab.com/a.bizzego/pyphysio/-/tree/master/tutorials>
     
### Reference
If you use `pyphysio` for research, please cite us:

"Bizzego et al. (2019) 'pyphysio: A physiological signal processing library for data science approaches in physiology', *SoftwareX*"
<https://www.sciencedirect.com/science/article/pii/S2352711019301839>
